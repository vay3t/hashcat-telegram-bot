from posixpath import basename
from lib import *
import os
import glob
import psutil
import validators


# poner emojis



def install_hashcat():
    dir_list_hashcat = glob.glob("hashcat-*")
    if len(dir_list_hashcat) == 0:
        bool_install = download_hashcat()
        if bool_install:
            return "hashcat installed"
        else:
            return "Failed to install hashcat"
    else:
        return "hashcat was installed"




def create_db_hashes():
    try:
        if check_installed_hashcat():
            change_dir_hashcat()
            
            if check_os() == "WSL":
                list_help = [" ".join(x.split()) for x in str(
                    subprocess.check_output([path_bin_hashcat(), "--help"])).split("\\r\\n")]
            elif check_os() == "Linux":
                list_help = [" ".join(x.split()) for x in str(
                    subprocess.check_output([path_bin_hashcat(), "--help"])).split("\\n")]

            new_list = find_between(
                list_help,
                "- [ Hash modes ] -",
                "- [ Brain Client Features ] -",
                4)
            with open("hashes.db", "w") as f:
                for x in new_list:
                    lista_hash_u = x.split("|")
                    if len(lista_hash_u) == 3:
                        stripped_list = [x.strip() for x in lista_hash_u]
                        f.write(
                            f"{stripped_list[0]}|{stripped_list[1]}|{stripped_list[2]}\n")

            if os.path.exists("hashes.db"):
                back_dir()
                return "Database created"
            else:
                back_dir()
                return "Database not created"

        else:
            return "Hashcat not installed"

    except Exception as e:
        return f"Error: {e}"




def install_wordlists():
    if not os.path.exists("wordlists"):
        os.mkdir("wordlists")
    # medio bug con el uniq wordlist, tengo que usar for para varias wordlists
    bool_install = download_wordlist_rockyou()
    if bool_install:
        return "wordlists installed"
    else:
        return "Failed to install wordlists"




def show_info():
    if check_installed_hashcat():
        text_response = []

        dir_hashcat = glob.glob("hashcat-*")[0]
        if os.path.exists(dir_hashcat):
            text_response.append(f"Hashcat version: {dir_hashcat}\n")
        else:
            text_response.append("[-] Hashcat not installed\n")

        if os.path.exists("wordlists"):
            filesname = glob.glob("wordlists/*")
            list_files = ""
            for i in filesname:
                list_files += f"    - {basename(i)}\n"
            text_response.append(f"wordlists/ uploaded:\n{list_files}")
        else:
            text_response.append("[-] Wordlists not uploaded\n")

        if os.path.exists("hashes"):
            filesname = glob.glob("hashes/*")
            list_files = ""
            for i in filesname:
                list_files += f"    - {basename(i)}\n"
            text_response.append(f"hashes/ uploaded:\n{list_files}")
        else:
            text_response.append("[-] Hashes not uploaded\n")

        if os.path.exists("masks"):
            filesname = glob.glob("masks/*")
            list_files = ""
            for i in filesname:
                list_files += f"    - {basename(i)}\n"
            text_response.append(f"masks/ uploaded:\n{list_files}")
        else:
            text_response.append("[-] Masks not uploaded")

        if os.path.exists("rules"):
            filesname = glob.glob("rules/*")
            list_files = ""
            for i in filesname:
                list_files += f"    - {basename(i)}\n"
            text_response.append(f"rules/ uploaded:\n{list_files}")
        else:
            text_response.append("[-] Rules not uploaded")

        if os.path.exists("potfiles"):
            filesname = glob.glob("potfiles/*")
            list_files = ""
            for i in filesname:
                list_files += f"    - {basename(i)}\n"
            text_response.append(f"potfiles/ exists:\n{list_files}")
        else:
            text_response.append("[-] Potfiles not exists")

        if os.path.exists("session"):
            filesname = glob.glob("session/*")
            list_files = ""
            for i in filesname:
                list_files += f"    - {basename(i)}\n"
            text_response.append(f"sessions/ exists:\n{list_files}")
        else:
            text_response.append("[-] Sessions not exists")

        return "\n".join(text_response)
    else:
        return "**Hashcat not installed**"




def list_hashes():  # se puede ocupar un ID para seleccionar el hash al igual que la wordlist
    if os.path.exists("hashes"):
        list_name_hashes = ""
        for i in show_list_hashes():
            list_name_hashes += (f"    {i}\n")
        return f"hashes/ uploaded:\n{list_name_hashes}"
    else:
        os.mkdir("hashes")
        return "Hashes dir created"




def benchmark_hash(hashid):
    change_dir_hashcat()
    hash_name = None
    with open("hashes.db", "r") as f:
        for line in f:
            if line.split("|")[0] == hashid:
                hash_name = line.split("|")[1]
                break
        if hash_name is None:
            return "Hash not found"

    output = subprocess.check_output(
        [path_bin_hashcat(), "-b", "-m", hashid]).decode("utf-8").split("\r\n")
    back_dir()
    return print_bench(output, hash_name)


def send_db_hashes():
    if glob.glob("hashcat-*/hashes.db"):
        change_dir_hashcat()
        return "hashes.db"
    else:
        return "hashes.db not found"


def grep_db_hashes(coincidence):
    if glob.glob("hashcat-*/hashes.db"):
        change_dir_hashcat()
        with open("hashes.db", "r") as f:
            list_hashes = f.readlines()
        back_dir()
        concat_coincidence = ""
        for line in list_hashes:
            if coincidence.lower() in line.lower():
                concat_coincidence += line
        return concat_coincidence
    else:
        return "hashes.db not found"


"""
# añadir soporte con async
def benchmark_full():
    change_dir_hashcat("hashcat_dir")
    output = subprocess.check_output([path_bin_hashcat(), "-b"]).decode("utf-8").split("\r\n")
    change_dir_hashcat("..")
    return print_bench(output)
"""

# parsear output cochino
def identify_hash(hashfile):
    change_dir_hashcat()
    output = subprocess.check_output(
        [path_bin_hashcat(), "--identify", hashfile]).decode("utf-8").split("\r\n")
    back_dir()
    string = ""
    for ele in output:
        if "|" in ele:
            new_ele = ele.replace("\r","")
            string += " ".join(new_ele.split())
            string += "\n"
    return string



# aux function for fastcrack_run
def hashid_exists(hashid):
    if glob.glob("hashcat-*/hashes.db"):
        change_dir_hashcat()
        with open("hashes.db", "r") as f:
            for line in f:
                if line.split("|")[0] == hashid:
                    back_dir()
                    return True
        back_dir()
        return False
    else:
        back_dir()
        return False

# in dev
def hashcat_run(
        hashid,
        hashfile,
        wordlist,
        update,
        session="",
        status_timer="60"
        ):

    if session == "":
        session = "hashcat"

    if not hashid.isdigit():
        update.message.reply_text("Hash ID must be a number")
        return
    
    else:
        if not hashid_exists(hashid):
            update.message.reply_text("Hash ID not found")
            return

    change_dir_hashcat() # tested... ok

    # cambiar para que los nuevos jobs no pisen el archivo
    name_file = vars_work_file(f"outputs/{session}.output")
    potfile = vars_work_file(f"potfiles/{session}.potfile")
    potfile = check_potfile_exists(potfile)

    with open(name_file, "w") as f:
        try:
            return_code = subprocess.run([
                path_bin_hashcat(),
                "-a", "0",
                "-m", hashid,
                "--session", session,
                "--status",
                "--status-timer", status_timer,
                "--potfile-path", potfile,
                hashfile,
                wordlist
            ], stdout=f, check=True)
            read_s = read_status(name_file)
            content_pot = read_pot(potfile)
            back_dir()
            if return_code.returncode == 0:
                update.message.reply_text(
                    f"Cracking finished\n{read_s}\n{content_pot}")
            else:
                update.message.reply_text(f"Cracking failed")
        except subprocess.CalledProcessError as e:
            back_dir()
            print(e)
            update.message.reply_text("Invalid command")




# funcion no probada
def restore_session(session, update):
    session_check = basename(session)
    if os.path.exists(f"../session/{session_check}"):
        change_dir_hashcat()
        name_file = f"../outputs/{session}.output"
        potfile = f"../potfiles/{session}.potfile"
        potfile = check_potfile_exists(potfile)
        with open(name_file, "a") as f:
            try:
                return_code = subprocess.run([
                    path_bin_hashcat(),
                    "--restore",
                    "--session", session
                ], stdout=f, check=True)

                read_s = read_status(name_file)
                content_pot = read_pot(potfile)
                back_dir()

                if return_code.returncode == 0:
                    update.message.reply_text(
                        f"Cracking finished\n{read_s}\n{content_pot}")
                    os.remove(f"session/{session_check}")
                else:
                    update.message.reply_text("Cracking failed")

            except subprocess.CalledProcessError as e:
                back_dir()
                print(e)
                update.message.reply_text("Invalid command")
    else:
        update.message.reply_text("Session not found")




# no probado
def clear_all_sessions():
    if os.path.exists("session"):
        for i in glob.glob("session/*"):
            os.remove(i)
        return "All sessions deleted"
    else:
        return "No session found"



# no probado
def clear_all_potfiles():
    if os.path.exists("potfiles"):
        for i in glob.glob("potfiles/*"):
            os.remove(i)
        return "All potfiles deleted"
    else:
        return "No potfiles found"



# no probado
def show_potfiles():
    if os.path.exists("potfiles"):
        list_name_pot = ""
        for i in glob.glob("potfiles/*"):
            with open(i, "r") as f:
                list_name_pot += (f"{i}\n")
        return f"Potfiles:\n{list_name_pot}"
    else:
        return "No potfiles found"



# no probado
def list_sessions():
    if os.path.exists("session"):
        list_name_sessions = ""
        for i in glob.glob("session/*"):
            list_name_sessions += f"    - {i}\n"
        return f"Sessions:\n{list_name_sessions}"
    else:
        return "No sessions found"



# no probado
def killall_hashcat():
    PROCNAME = path_bin_hashcat()
    for proc in psutil.process_iter():
        # check whether the process name matches
        if PROCNAME in proc.name():
            proc.kill()
    return "All hashcat processes killed"



# no probado
def actual_status():
    list_files = glob.glob("outputs/*.output")
    dict_files_time = {}
    if list_files:
        for i in list_files:
            dict_files_time.update({i: os.path.getmtime(i)})
        dict_files_time = sorted(
            list_files.items(),
            key=lambda x: x[1],
            reverse=True)
        newerfile = dict_files_time[0][0]
        return read_status(newerfile)
    else:
        return "No status found"


def fastcrack_run(hashtext):
    from search_that_hash import api
    parsed_info = api.return_as_json([hashtext])[0][hashtext]
    decrypted = "null"
    type_hash = "null"
    if parsed_info:
        for i in parsed_info:
            for k,v in i.items():
                if k == "hashdecryption":
                    decrypted = v
                if k == "Type":
                    type_hash = v

        return f"{hashtext} - {type_hash} - {decrypted}"
    else:
        return "The API of search-that-hash have a problem."


# Bypass limit download telegram
def downloader_wordlist(url):
    try:
        if validators.url(url):
            r = requests.get(url,stream=True)
            if r.status_code == 200:
                r_size = int(r.headers['Content-length'])
                if "lst" or "txt" in url.split("/")[-1].split(".")[-1]:
                    if not os.path.exists(url.split("/")[-1]):
                        if r_size < 2000000000:
                            print("File size is smaller than 2GB")
                            r_download = requests.get(url)
                            filename = url.split("/")[-1]
                            path_wordlist = "wordlists/" + filename
                            if r_download.status_code == 200:
                                with open(f"{path_wordlist}", "wb") as f:
                                    f.write(r_download.content)
                                    if os.path.getsize(path_wordlist) == r_size:
                                        return "Wordlist downloaded successfully"
                                    else:
                                        return "Wordlist downloaded with problems"
                            else:
                                return "Cant download wordlist"
                        else:
                            return "File size is larger than 2GB"
                    else:
                        return "File already exists"
                else:
                    return "File is not a wordlist. Only 'lst' or 'txt' extension"
            else:
                return "Cant download wordlist"
        else:
            return "Invalid URL"
    except Exception as e:
        print(e)
        return "Cant download wordlist"