import os
import subprocess
import glob
import requests
import shutil

# Descargar wordlist por torrent
# https://gist.github.com/samukasmk/940ca5d5abd9019e8b1af77c819e4ca9 libtorrent
# sudo apt install libtorrent-dev

# corregir directorio de trabajo para cuando no se vaya a la B
# cuando ejecuto ../main.py


def check_os():
    if os.name == 'nt':
        return 'This script is not compatible with Windows for now.'
    else:
        if os.name == "posix":
            if shutil.which("cmd.exe"):
                return "WSL"
            else:
                return "Linux"
        else:
            return "Unknown OS"


def check_installed_hashcat():
    if glob.glob("hashcat-*"):
        return True

def download_hashcat():
    hashcat_version = requests.get(
        "https://github.com/hashcat/hashcat/releases/latest",
        allow_redirects=False).text.split('"')[1].split("/")[-1]
    r = requests.get(
        f"https://github.com/hashcat/hashcat/releases/download/{hashcat_version}/hashcat-{hashcat_version.strip('v')}.7z",
        allow_redirects=True)
    with open('hashcat.7z', 'wb') as f:
        f.write(r.content)
    subprocess.run(["7z", "x", "hashcat.7z"])
    os.remove("hashcat.7z")
    dir_hashcat = glob.glob("hashcat-*")[0]
    subprocess.run(["chmod","-R","+x",dir_hashcat])
    if os.path.exists(dir_hashcat):
        return True


def download_wordlist_rockyou():
    if os.path.exists("wordlists/rockyou.txt"):
        return True
    else:
        r = requests.get(
            "https://github.com/brannondorsey/naive-hashcat/releases/download/data/rockyou.txt")
        with open('wordlists/rockyou.txt', 'wb') as f:
            f.write(r.content)
        if os.path.exists("wordlists/rockyou.txt"):
            return True


def show_list_hashes():
    hashes = glob.glob("hashes/*.hash")
    hashes = [os.path.basename(x) for x in hashes]
    return hashes


def create_dirs():
    if not os.path.exists("wordlists"):
        os.mkdir("wordlists")
    if not os.path.exists("hashes"):
        os.mkdir("hashes")
    if not os.path.exists("masks"):
        os.mkdir("masks")
    if not os.path.exists("rules"):
        os.mkdir("rules")
    if not os.path.exists("potfiles"):
        os.mkdir("potfiles")
    if not os.path.exists("sessions"):
        os.mkdir("sessions")

    if not os.path.exists("outputs"):
        os.mkdir("outputs")
    if not os.path.exists("tmp"):
        os.mkdir("tmp")


def back_dir():
    os.chdir("..")


def change_dir_hashcat():
    if glob.glob("hashcat-*"):
        os.chdir(glob.glob("hashcat-*")[0])
    else:
        return False


def find_between(s, first, last, offset):
    try:
        start = s.index(first)
        end = s.index(last)
        string = s[start + offset:end]
        return string
    except ValueError:
        return ""


def path_bin_hashcat():
    if check_os() == "WSL":
        bin_hashcat = "./hashcat.exe"
        os.getcwd()  # borrar
        if os.path.exists(bin_hashcat):
            return bin_hashcat
        else:
            return False
    elif check_os() == "Linux":
        bin_hashcat = "./hashcat.bin"
        if os.path.exists(bin_hashcat):
            return bin_hashcat
        else:
            return False
    else:
        return False


def print_bench(output_bench, hash_name):
    for x in output_bench:
        if "Speed" in x:
            info = x.split()
            return f"{hash_name}: {info[0]} {info[1]} {info[2]}"


def printer_status(last_lines):
    for line in last_lines:
        if "Session..........:" in line:
            break

    i = last_lines.index(line)
    c = 0
    printer = ""
    while c < 21:
        line_while = last_lines[i + c]
        printer += line_while
        c += 1

    return printer


def read_status(output):
    with open(output, "r") as f:
        read_log = f.readlines()
    if "Compared hashes with potfile entries\n" in read_log:
        return "Hashes in potfile"
    else:
        last_lines = read_log[-30:]
        return printer_status(last_lines)


def check_potfile_exists(potfile):
    a = 1
    while a < 1000:
        if os.path.exists(potfile):
            new_digit = "{:03d}".format(a)
            new_pot = potfile.replace(".potfile", f"_{new_digit}.potfile")
            return new_pot
        else:
            a += 1
            return potfile

def check_session_exists(session_name):
    a = 1
    while a < 100:
        if os.path.exists(session_name):
            new_pot = session_name.replace(".session", f"_{a.zfill(2)}.session")
            return new_pot
        else:
            a += 1
            return session_name    


def read_pot(potfile):
    with open(potfile, "r") as f:
        read_pot = f.read()
    return read_pot


def concat_work_file(dir_work, file_work):
    return f"../{dir_work}/{file_work}"


def vars_work_file(parameter):
    list_parameters = parameter.split("/")
    if len(list_parameters) == 2:
        var = concat_work_file(list_parameters[0], list_parameters[1])
        return var
    else:
        return False
