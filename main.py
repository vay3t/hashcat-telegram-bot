#!/usr/bin/env python
# -*- coding: utf-8 -*-
# This program is dedicated to the public domain under the CC0 license.
#
# THIS EXAMPLE HAS BEEN UPDATED TO WORK WITH THE BETA VERSION 12 OF PYTHON-TELEGRAM-BOT.
# If you're still using version 11.1.0, please see the examples at
# https://github.com/python-telegram-bot/python-telegram-bot/tree/v11.1.0/examples

"""
Simple Bot to reply to Telegram messages.

First, a few handler functions are defined. Then, those functions are passed to
the Dispatcher and registered at their respective places.
Then, the bot is started and runs until we press Ctrl-C on the command line.

Usage:
Basic Echobot example, repeats messages.
Press Ctrl-C on the command line or send a signal to the process to stop the
bot.
"""

from functools import wraps
from lib import create_dirs, check_installed_hashcat
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters
from environs import Env
from core import *
import logging

env = Env()
env.read_env()

TOKEN_BOT = env.str('TOKEN_BOT')
LIST_OF_ADMINS = env.list("LIST_OF_ADMINS")

LIST_ADMINS_INT = [int(x) for x in LIST_OF_ADMINS]

# Enable logging
logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
    level=logging.INFO)

logger = logging.getLogger(__name__)

create_dirs()


def restricted(func):
    @wraps(func)
    def wrapped(update, context, *args, **kwargs):
        user_id = update.effective_user.id
        print(update.effective_user)
        if not user_id in LIST_ADMINS_INT:
            print(f"{user_id} not in {LIST_ADMINS_INT}")
            print("Unauthorized access denied for {}:\nmessage: {}.".format(str(update.effective_user),str(context.args)))
            return
        return func(update, context, *args, **kwargs)
    return wrapped


# Define a few command handlers. These usually take the two arguments bot and
# update. Error handlers also receive the raised TelegramError object in error.

@restricted
def start(update, context):
    """Send a message when the command /start is issued."""
    update.message.reply_text('Hi!')


@restricted
def help(update, context):
    """Send a message when the command /help is issued."""
    texto = """/help - Show this help
/install <hashcat/wordlist/gendb/all> - Install hashcat, wordlist or generate db in server
/info - Show info about hashcat and utils

/fastcrack <HASH> - Crack hash online

### Max 20MB files
/addhashfile <UPLOAD-HASHFILE> - Upload hash file
/addwordlist <UPLOAD-WORDLISTFILE> - Upload wordlist
/addmask <UPLOAD-MASKFILE> - Upload file masks
/addrules <UPLOAD-RULESFILE> - Upload file rules
###

/identify <UPLOAD-HASHFILE> - Identify hash in upload
/identify <HASH> - Identify hash in message

/gethashlist - Download hash list
/benchmark <IDHASH> - Benchmark of hash
/grephashid <STRING> - Grep hash id list

/hashcat <HASHID> <HASHFILESTORED> <WORDLIST> - Fast crack usage
/status - Status of actual job
/potfiles - Show all passwords cracked
/listsessions - List all sessions
/killall confirm - Kill actual job

/wget <URL> - Download wordlist from url (Disk Limit)

# not work /restore <SESSIONNAME> - Restore session
# not work /clearsessions confirm - Delete all sessions
# not work /clearpotfile - Delete all passwords cracked
# not work /wipe all - Wipe all files [NOT IMPLEMENTED]
	"""
    update.message.reply_text(texto)


@restricted
def error(update, context):
    """Log Errors caused by Updates."""
    logger.warning('Update "%s" caused error "%s"', update, context.error)


@restricted
def install(update, context):
    """Install hashcat in server"""
    try:
        if context.args[0] == "hashcat":
            logger.info("Install hashcat in server")
            update.message.reply_text("Installing hashcat in server")
            update.message.reply_text(install_hashcat())

        elif context.args[0] == "wordlists":
            logger.info("Install wordlists in server")
            update.message.reply_text("Installing wordlist in server")
            update.message.reply_text(install_wordlists())

        elif context.args[0] == "gendb":
            logger.info("Generate db in server")
            update.message.reply_text("Generating db in server")
            update.message.reply_text(create_db_hashes())

        elif context.args[0] == "all":
            logger.info("Install all in server")
            update.message.reply_text("Installing all in server")

            logger.info("Install hashcat in server")
            update.message.reply_text("Installing hashcat in server")
            update.message.reply_text(install_hashcat())

            logger.info("Install wordlists in server")
            update.message.reply_text("Installing wordlist in server")
            update.message.reply_text(install_wordlists())

            logger.info("Generate db in server")
            update.message.reply_text("Generating db in server")
            update.message.reply_text(create_db_hashes())

            update.message.reply_text("Finished")

        else:
            update.message.reply_text("Invalid command")

    except (ValueError, IndexError):
        update.message.reply_text("Invalid command")


@restricted
def info(update, context):
    """Show info about hashcat or wordlist"""
    update.message.reply_text(show_info())


# dejar con async
@restricted
def upload_file(update, context):
    try:
        if update.message.caption == "/addhashfile":
            """Upload hash file"""
            fileID = update.message.document.file_id
            newFile = context.bot.get_file(fileID)
            os.chdir(os.path.join(os.getcwd(), "hashes"))
            newFile.download(update.message.document.file_name)
            os.chdir(os.path.join(os.getcwd(), ".."))
            update.message.reply_text("Hash file uploaded")

        # añadir soporte con async
        # Problema archivo es muy grande
        # interesante seria añadir enlace simbolico
        elif update.message.caption == "/addwordlist":
            """Upload wordlist file"""
            fileID = update.message.document.file_id
            newFile = context.bot.get_file(fileID)
            os.chdir(os.path.join(os.getcwd(), "wordlists"))
            newFile.download(update.message.document.file_name)
            os.chdir(os.path.join(os.getcwd(), ".."))
            update.message.reply_text("wordlist file uploaded")

        elif update.message.caption == "/addmask":
            """Upload mask file"""
            fileID = update.message.document.file_id
            newFile = context.bot.get_file(fileID)
            os.chdir(os.path.join(os.getcwd(), "masks"))
            if update.message.document.file_name.split(".")[-1] == "hcmask":
                newFile.download(update.message.document.file_name)
                os.chdir(os.path.join(os.getcwd(), ".."))
                update.message.reply_text("Mask file uploaded")
            else:
                os.chdir(os.path.join(os.getcwd(), ".."))
                update.message.reply_text("Invalid file")

        elif update.message.caption == "/addrule":
            """Upload rules file"""
            fileID = update.message.document.file_id
            newFile = context.bot.get_file(fileID)
            os.chdir(os.path.join(os.getcwd(), "rules"))
            if update.message.document.file_name.split(".")[-1] == "rule":
                newFile.download(update.message.document.file_name)
                os.chdir(os.path.join(os.getcwd(), ".."))
                update.message.reply_text("Rule file uploaded")
            else:
                os.chdir(os.path.join(os.getcwd(), ".."))
                update.message.reply_text("Invalid file")

        elif update.message.caption == "/identify":
            """Identify hash in upload"""
            os.chdir("tmp")
            fileID = update.message.document.file_id
            newFile = context.bot.get_file(fileID)
            newFile.download(update.message.document.file_name)
            not_full_path = "../" + \
                "/".join(os.path.abspath(update.message.document.file_name).split("/")[-2:])
            back_dir()
            update.message.reply_text(identify_hash(not_full_path))
            os.remove(not_full_path)

    except (ValueError):
        update.message.reply_text("Invalid command")


# dejarlo con async
@restricted
def benchmark(update, context):
    try:
        if check_installed_hashcat():
            hashid = context.args[0]

            if hashid.isdigit():
                update.message.reply_text("Benchmark running...")
                update.message.reply_text(benchmark_hash(hashid))

                """
            elif hashid == "all":
                update.message.reply_text("Benchmark running...")
                change_dir_hashcat("hashcat_dir")
                output = subprocess.check_output([path_bin_hashcat(), "-b"])
                update.message.reply_text(output)
                change_dir_hashcat("back_dir")

                update.message.reply_text("Benchmark running...")
                """

            else:
                update.message.reply_text("Invalid command")

        else:
            update.message.reply_text("Hashcat not installed")

    except (ValueError):
        update.message.reply_text("Invalid command")


@restricted
def gethashlist(update, context):
    try:
        hashesdb = glob.glob("hashcat-*/hashes.db")[0]
        if hashesdb:
            update.message.reply_document(open(hashesdb, "rb"))
        else:
            update.message.reply_text("No hashes db exist")
    except (ValueError):
        update.message.reply_text("Invalid command")


@restricted
def grephashid(update, context):
    try:
        coincidences = " ".join(context.args)
        update.message.reply_text(grep_db_hashes(coincidences))
    except (ValueError):
        update.message.reply_text("Invalid command")


# ya esta con async
@restricted
def hashcat(update, context):
    try:
        hashid = context.args[0]
        hashfile = vars_work_file(context.args[1])
        wordlist = vars_work_file(context.args[2])

        if not hashid:
            update.message.reply_text("Invalid hashid")

        if not hashfile:
            update.message.reply_text("Invalid hash file")

        if not wordlist:
            update.message.reply_text("Invalid wordlist")

        if hashid.isdigit() and hashfile and wordlist:
            try:
                update.message.reply_text("[!] BREAKPOINT")
                context.dispatcher.run_async(
                    hashcat_run, hashid, hashfile, wordlist, update)
            except Exception as e:
                print(e)
                update.message.reply_text("Error running hashcat")
        
        else:
            update.message.reply_text("Invalid algo")
                
    except (ValueError):
        update.message.reply_text("Invalid command")


# funcion no probada
@restricted
def restore(update, context):
    try:
        sessionfile = context.args[0]
        context.dispatcher.run_async(restore_session, sessionfile, update)
    except BaseException:
        update.message.reply_text("Invalid command")
    return


@restricted
def clearsessions(update, context):
    try:
        if context.args[0] == "confirm":
            update.message.reply_text(clear_all_sessions())
        else:
            update.message.reply_text("Please try '/clearsessions confirm'")
    except BaseException:
        update.message.reply_text("Invalid command")


@restricted
def clearpotfiles(update, context):
    try:
        if context.args[0] == "confirm":
            update.message.reply_text(clear_all_potfiles())
        else:
            update.message.reply_text("Please try '/clearpotfiles confirm'")
    except BaseException:
        update.message.reply_text("Invalid command")


@restricted
def potfiles(update, context):
    try:
        potfiles = glob.glob("potfiles/*.pot")
        if potfiles:
            update.message.reply_text(show_potfiles())
        else:
            update.message.reply_text("No potfiles exist")
    except BaseException:
        update.message.reply_text("Invalid command")


@restricted
def killall(update, context):
    try:
        if context.args[0] == "confirm":
            update.message.reply_text(killall_hashcat())
        else:
            update.message.reply_text("Please try '/killall confirm'")
    except BaseException:
        update.message.reply_text("Invalid command")


@restricted
def status(update, context):
    try:
        update.message.reply_text(actual_status())
    except BaseException:
        update.message.reply_text("Invalid command")


@restricted
def fastcrack(update, context):
    """Fastcrack"""
    try:
        hashtext = str(context.args[0])
        update.message.reply_text(fastcrack_run(hashtext))
    except BaseException:
        update.message.reply_text("Invalid command")


@restricted
def identify(update,context):
    """Identify hardcoded"""
    try:
        hashtext = context.args[0]
        update.message.reply_text("Idenfinding...")
        logger.info("go go go!")
        output = identify_hash(hashtext)
        update.message.reply_text(output)
    except BaseException:
        update.message.reply_text("Invalid command")


@restricted
def wget(update,context):
    """wget"""
    try:
        url = context.args[0]
        update.message.reply_text("Downloading wordlist...")
        logger.info(f"Downloading wordlist: {url}")
        output = downloader_wordlist(url)
        update.message.reply_text(output)
    except BaseException:
        update.message.reply_text("Invalid command")


def main():
    """Start the bot."""
    # Create the Updater and pass it your bot's token.
    # Make sure to set use_context=True to use the new context based callbacks
    # Post version 12 this will no longer be necessary
    updater = Updater(TOKEN_BOT, use_context=True)

    # Get the dispatcher to register handlers
    dp = updater.dispatcher

    # on different commands - answer in Telegram
    dp.add_handler(CommandHandler("start", start))
    dp.add_handler(CommandHandler("help", help))
    dp.add_handler(CommandHandler("install", install))
    dp.add_handler(CommandHandler("info", info))
    dp.add_handler(MessageHandler(Filters.document, upload_file))
    dp.add_handler(CommandHandler("benchmark", benchmark))
    dp.add_handler(CommandHandler("gethashlist", gethashlist))
    dp.add_handler(CommandHandler("grephashid", grephashid))
    dp.add_handler(CommandHandler("hashcat", hashcat))
    # dp.add_handler(CommandHandler("restore", restore)) # no probado

    # Funciones desabilitadas por ser peligrosas.
    """
    dp.add_handler(
        CommandHandler(
            "clearsessions",
            clearsessions))  # no probado

    dp.add_handler(
        CommandHandler(
            "clearpotfiles",
            clearpotfiles))  # no probado
    """

    dp.add_handler(CommandHandler("potfiles", potfiles))  # no probado
    dp.add_handler(CommandHandler("killall", killall))  # no probado
    dp.add_handler(CommandHandler("status", status))  # no probado
    dp.add_handler(CommandHandler("fastcrack", fastcrack))
    dp.add_handler(CommandHandler("identify", identify))
    dp.add_handler(CommandHandler("wget", wget))


    # log all errors
    dp.add_error_handler(error)

    # Start the Bot
    updater.start_polling()

    # Run the bot until you press Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    updater.idle()


if __name__ == '__main__':
    main()
